﻿## KeyStorage
Composer пакет для Laravel 5.*. Для того, чтобы хранить ключ => значение в БД.

**Поддерживает объекты которые можно сериализовать и массивы**

## Установка пакета
**Шаг 1:**  Добавить в **composer.json** репозиторий с модулем.

```json
...
"repositories": [
    {
      "packagist.org": false,
      "type": "vcs",
      "url": "git@bitbucket.org:genioteamerp/keystorage.git"
    }
  ]
  ...
```

**Шаг 2:**  Добавить сам пакет в **composer.json**


```json
...
  "require": {
    ...
    "genioteam/keystorage": "1.1.0"
  }
```

**Шаг 3:**  Добавить провайдера в **config/app.php**

```php
'providers' => [
    ...
    Genioteam\KeyStorage\KeyStorageProvider::class,
    ...
 ];
```

**Шаг 4:**  Запустить Artisan комманду vendor:publish и migrate

```bash
php artisan vendor:publish --provider="Genioteam\KeyStorage\KeyStorageProvider" --force
php artisan migrate
```
## Пример использования
```php
KeyStorage::set('test',['password'=>'qwer123','login'=>'admin']);
KeyStorage::set('test2',new Object());

KeyStorage::get('test')['password'] // qwer123
KeyStorage::get('test')['login'] // admin

KeyStorage::get('test2') // Object
```