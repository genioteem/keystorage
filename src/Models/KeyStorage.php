<?php

namespace Genioteam\KeyStorage\Models;

use Illuminate\Database\Eloquent\Model;

class KeyStorage extends Model
{
    protected $table = 'key_storage';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public $timestamps = true;



    public static function set($key, $type, $value){
        $model = self::where('key',$key)->first();

        if (!$model){
            return self::create(['key'=>$key,'type'=>$type,'value'=>$value]);
        }
        $model->update(['value'=>$value,'type'=>$type]);
        return $model;
    }


}
