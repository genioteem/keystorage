<?php

namespace Genioteam\KeyStorage;

use Genioteam\KeyStorage\Models\KeyStorage as KeyStorageModel;

class KeyStorageSingleton{

    protected $storage = array();

    public function getStorage(){
        return $this;
    }

    public function updateStorage()
    {
        $storage = array();

        try {
            foreach (KeyStorageModel::all() as $item) {
                $value = $item->value;
                $value = $this->handlGet($item->type, $value);
                $this->storage[$item->key] = ['value' => $value, 'type' => $item->type];
            }
        } catch (\Illuminate\Database\QueryException $e) {
            // 2002 - нету подключения с базой
        }
    }

    public function set($key,$value){
        $this->storage[$key]['value'] = $value;
        $type = gettype($value);
        $value = $this->handlSet($value);
        KeyStorageModel::set($key, $type, $value);

        return $this;
    }

    public function get($key){
        if (!key_exists($key,$this->storage)){
            return false;
        }
        return $this->storage[$key]['value'];
    }

    public function getType($key){
        return gettype($this->get($key));
    }

    protected function handlSet($value){
        $type = gettype($value);
        switch ($type) {
            case 'array':
                return serialize($value);
            case 'object':
                return serialize($value);
            default:
                return (string)$value;
        }
    }
    protected function handlGet($type, $value){
        switch ($type) {
            case 'array':
                return unserialize($value);
            case 'object':
                return unserialize($value);
            default:
                settype($value, $type);
                return $value;
        }
    }
}