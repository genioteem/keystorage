<?php

namespace Genioteam\KeyStorage\Facades;

use Illuminate\Support\Facades\Facade;

class KeyStorage extends Facade{


    protected static function getFacadeAccessor()
    {
        return 'KeyStorage';
    }

    protected static function get($key)
    {
        return 'KeyStorage';
    }
    

}