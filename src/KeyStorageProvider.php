<?php

namespace Genioteam\KeyStorage;


//@todo изменить пространство имен 
use Genioteam\KeyStorage\Facades\KeyStorage;
use Genioteam\KeyStorage\KeyStorageSingleton;

use Illuminate\Support\ServiceProvider;

class KeyStorageProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        KeyStorage::updateStorage();

        $this->publishes([
                __DIR__.'/../database/migrations/' => database_path('migrations')
            ], 'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('KeyStorage', function ($app) {
            return new KeyStorageSingleton();
        });
    }
}
